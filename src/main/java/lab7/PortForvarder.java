package lab7;

import lab7.my_socket_channel.MyChannelType;
import lab7.my_socket_channel.MySocketChannel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class PortForvarder {
    private ArrayList<MySocketChannel> mySocketChannels = new ArrayList<>();
    private ArrayList<MySocketChannel> mySocketChannelsOnRemove = new ArrayList<>();
    private Selector selector;
    private int lport = 10080;
    private int rport = 80;
    // piwigo
    private String rhost = "87.98.147.22";
    //picabu
    //private String rhost = "212.224.112.193";
    private SocketAddress serverSocketAddress = new InetSocketAddress(rhost, rport);
    private ServerSocketChannel serverSocketChannel;
    //private boolean firstRequest = false;


    public void work() {
        try {
            selector = Selector.open();
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(lport));
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

            while (true) {
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                //TODO без таймера не робит (на 20)
                int num = selector.select();
                System.out.println("NEXT SELECT " + num);

                Set selectedKeys = selector.selectedKeys();
                Iterator it = selectedKeys.iterator();
                while (it.hasNext()) {
                    SelectionKey curKey = (SelectionKey) it.next();
                    it.remove();
                    if ((curKey.isAcceptable())) {
                        startConnection();
                    } else {
                        SocketChannel curSocketChannel = (SocketChannel) curKey.channel();
                        MySocketChannel mySocketChannel = findAssociatedMySocketChannel(curSocketChannel);
                        if (curKey.isConnectable()) {
                            try {
                                if (curSocketChannel.finishConnect()) {
                                    System.out.println("finish connect");
                                    if (mySocketChannel.isOutputBufferEmpty()) {
                                        curKey.interestOps(SelectionKey.OP_READ);
                                    } else {
                                        curKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                                    }
                                }
                            } catch (IOException e) {
                                removeChannel(mySocketChannel);
                                removeChannel(mySocketChannel.getPairChanel());
                            }
                        }
                        if (curKey.isWritable()) {
                            writeData(curKey, mySocketChannel);
                        }
                        if (curKey.isReadable()) {
                            readData(curKey, mySocketChannel);
                        }
                    }
                }
                removeFromList();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MySocketChannel findAssociatedMySocketChannel(SocketChannel curSocketChannel) {
        MySocketChannel mySocketChannel = null;
        for (MySocketChannel msc : mySocketChannels) {
            if (msc.isItYourChannel(curSocketChannel)) {
                mySocketChannel = msc;
                break;
            }
        }
        return mySocketChannel;
    }


    private void writeData(SelectionKey curKey, MySocketChannel mySocketChannel) throws IOException {
        MySocketChannel pairChannel = mySocketChannel.getPairChanel();
        if (mySocketChannel.isOutputBufferEmpty()) {
            mySocketChannel.removeInterestOps(selector, SelectionKey.OP_WRITE);
            return;
        }
        mySocketChannel.writeFromBufferToSocketChannel();
        if (mySocketChannel.isOutputBufferFull()) {
            return;
        }
        // в буфере есть свободное место
        if (!pairChannel.isEof()) {
            pairChannel.addToInterestOps(selector, SelectionKey.OP_READ);
        }
        if (mySocketChannel.isOutputBufferEmpty()) {
            System.out.println("отписка с записи");
            mySocketChannel.removeInterestOps(selector, SelectionKey.OP_WRITE);
        }
    }


    private void readData(SelectionKey curKey, MySocketChannel mySocketChannel) throws IOException {
        MySocketChannel pairChannel = mySocketChannel.getPairChanel();
//        while (true) {
            int readBytes = pairChannel.writeToBufferFromChannel(mySocketChannel);
            if (pairChannel.isConnected()) {
                pairChannel.writeFromBufferToSocketChannel();
            } else {
                System.out.println("not yet connected so no write data");
            }
//            if (readBytes == 0) {
//                break;
//            }
            if (readBytes == -1) {
                System.out.println("GET -1 " + mySocketChannel.getType());
                mySocketChannel.setEof(true);
            }
            if (!pairChannel.isOutputBufferEmpty()) {
                System.out.println("подписка на запись");
                if (pairChannel.isConnected()) {
                    pairChannel.addToInterestOps(selector, SelectionKey.OP_WRITE);
                }
            }
            System.out.println(mySocketChannel.getType() + " readed: " + readBytes);
            //pairChannel.printBuffer();
            if (mySocketChannel.isEof()) {
                if (pairChannel.isEof() && mySocketChannel.isOutputBufferEmpty() &&
                        pairChannel.isOutputBufferEmpty()) {
                    removeChannel(mySocketChannel);
                    removeChannel(pairChannel);
//                    break;
                }
                if (pairChannel.isOutputBufferEmpty()) {
                    pairChannel.sendEof();
                    mySocketChannel.removeInterestOps(selector, SelectionKey.OP_READ);
//                    break;
                }
            }


            if (pairChannel.isOutputBufferFull()) {
                mySocketChannel.removeInterestOps(selector, SelectionKey.OP_READ);
            }

//        }
    }

    private void removeChannel(MySocketChannel mySocketChannel) {
        if (mySocketChannel != null && !mySocketChannelsOnRemove.contains(mySocketChannel)) {
            System.out.println("add to remove");
            mySocketChannelsOnRemove.add(mySocketChannel);
        }
    }

    private void removeFromList() throws IOException {
        for (MySocketChannel mySocketChannel : mySocketChannelsOnRemove) {
            mySocketChannel.close();
        }
        mySocketChannels.removeAll(mySocketChannelsOnRemove);
        mySocketChannelsOnRemove.clear();
    }

/*    private void changeHost(String rhost, ByteBuffer byteBuffer) {
        firstRequest = false;
        String requestString = new String(byteBuffer.array(), StandardCharsets.UTF_8);
        System.out.println("Do: " + requestString);
        requestString = requestString.replaceFirst("localhost:10080", rhost);
        System.out.println("posle: " + requestString);
        byteBuffer.clear();
        byteBuffer.put(requestString.getBytes(StandardCharsets.UTF_8));
    }*/

    private void startConnection() throws IOException {
        System.out.println("new Accept");
        SocketChannel clientChannel = serverSocketChannel.accept();
        clientChannel.configureBlocking(false);
        MySocketChannel myClientChannel = new MySocketChannel(clientChannel, MyChannelType.CLIENT);
        SocketChannel serverChannel = SocketChannel.open();
        serverChannel.configureBlocking(false);
        MySocketChannel myServerChannel = new MySocketChannel(serverChannel, MyChannelType.SERVER, myClientChannel);
        myClientChannel.setPairChanel(myServerChannel);
        mySocketChannels.add(myClientChannel);
        mySocketChannels.add(myServerChannel);
        clientChannel.register(selector, SelectionKey.OP_READ);
        if (serverChannel.connect(serverSocketAddress)) {
            serverChannel.register(selector, SelectionKey.OP_READ);
            System.out.println("instant connection");
        } else {
            serverChannel.register(selector, SelectionKey.OP_CONNECT);
        }
    }
}
