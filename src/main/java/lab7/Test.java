package lab7;
//Created by Evgeny on 12.12.2017.

import java.nio.ByteBuffer;

public class Test {
    public static void main(String[] args) {
        byte[] byti = {0, 1, 2, 3};
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        System.out.println(byteBuffer.position() + " p - l " + byteBuffer.limit());
        System.out.println(byteBuffer.remaining());
        byteBuffer.put(byti);
        System.out.println(byteBuffer.position() + " p - l " + byteBuffer.limit());
        System.out.println(byteBuffer.remaining());
    }
}
