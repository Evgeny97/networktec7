package lab7.my_socket_channel;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class MySocketChannel {

    private SocketChannel socketChannel;
    private MyChannelType type;
    private CurrentBufferMode currentMode;
    private MySocketChannel pairChanel;
    private ByteBuffer outputByteBuffer;
    private boolean eof;

    public MySocketChannel(SocketChannel socketChannel, MyChannelType type) {
        this(socketChannel, type, null);
    }

    public MySocketChannel(SocketChannel socketChannel, MyChannelType type, MySocketChannel pairChanel) {
        this.socketChannel = socketChannel;
        this.type = type;
        this.pairChanel = pairChanel;
        eof = false;
        outputByteBuffer = ByteBuffer.allocate(1024);
        currentMode = CurrentBufferMode.TO_BUFFER;
    }

    public boolean isEof() {
        return eof;
    }

    public void setEof(boolean eof) {
        this.eof = eof;
    }

    public int writeToBufferFromChannel(MySocketChannel mySocketChannel) throws IOException {
        if (currentMode != CurrentBufferMode.TO_BUFFER) {
            outputByteBuffer.compact();
            currentMode = CurrentBufferMode.TO_BUFFER;
        }
        int read = mySocketChannel.socketChannel.read(outputByteBuffer);
        return read;
    }

    public boolean isConnected() {
        return socketChannel.isConnected();
    }

    public void close() throws IOException {
        socketChannel.close();
    }

    public void printBuffer() {
        System.out.println(new String(outputByteBuffer.array(), StandardCharsets.UTF_8));
    }

    public int writeFromBufferToSocketChannel() throws IOException {
        if (currentMode != CurrentBufferMode.FROM_BUFFER) {
            outputByteBuffer.flip();
            currentMode = CurrentBufferMode.FROM_BUFFER;
        }
        int write = socketChannel.write(outputByteBuffer);
//        outputByteBuffer.compact();
//        currentMode = CurrentBufferMode.TO_BUFFER;
        return write;
    }

    public void sendEof() throws IOException {
        socketChannel.shutdownOutput();
    }

    public int getInterestOps(Selector selector) {
        return socketChannel.keyFor(selector).interestOps();
    }

    public void setInterestOps(Selector selector, int ops) {
        socketChannel.keyFor(selector).interestOps(ops);
    }

    public void addToInterestOps(Selector selector, int ops) {
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        selectionKey.interestOps(selectionKey.interestOps() | ops);
    }
    public void removeInterestOps(Selector selector, int ops){
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        selectionKey.interestOps(selectionKey.interestOps() & ~ops);
    }

    public boolean isOutputBufferFull() throws IOException {
        if (currentMode == CurrentBufferMode.FROM_BUFFER) {
            return outputByteBuffer.remaining() == outputByteBuffer.capacity();
        } else if (currentMode == CurrentBufferMode.TO_BUFFER) {
            return outputByteBuffer.remaining() == 0;
        }
        throw new IOException("can't see this");
    }

    public boolean isOutputBufferEmpty() throws IOException {
        if (currentMode == CurrentBufferMode.FROM_BUFFER) {
            return outputByteBuffer.remaining() == 0;
        } else if (currentMode == CurrentBufferMode.TO_BUFFER) {
            return outputByteBuffer.position() == 0;
        }
        throw new IOException("can't see this");
    }

//    public ByteBuffer getOutputByteBuffer() {
//        return outputByteBuffer;
//    }

//    public SocketChannel getSocketChannel() {
//        return socketChannel;
//    }

    public boolean isItYourChannel(SocketChannel socketChannel) {
        return socketChannel == this.socketChannel;
    }

    public void registerSocketChanel(Selector selector, int ops) throws ClosedChannelException {
        socketChannel.register(selector, ops);
    }

    public MyChannelType getType() {
        return type;
    }


    public MySocketChannel getPairChanel() {
        return pairChanel;
    }

    public void setPairChanel(MySocketChannel pairChanel) {
        this.pairChanel = pairChanel;
    }
}

